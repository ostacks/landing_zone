# the base set of admin roles
resource "oci_identity_group" "admins" {
    provider       = oci.home
    for_each       = var.admins

    #Required
    compartment_id  = var.tenancy_id
    name            = each.key
    description     = "group for the ${each.key} role"

    #Optional
    # defined_tags  = {"Operations.CostCenter"= "42"}
    freeform_tags   = {"Platform"=var.platform_name, "Environment"=var.label_prefix}
}

resource "oci_identity_policy" "admins" {
    provider       = oci.home
    depends_on     = [ oci_identity_group.admins ]
    for_each       = var.admins

    #Required
    compartment_id = var.tenancy_id
    name           = "${each.key}_admin"
    description    = "Policies for the ${each.key} admininistrrator"
    statements     = each.value
}

# create an read_only role
resource "oci_identity_group" "read_only" {
    provider        = oci.home

    #Required
    compartment_id  = var.tenancy_id
    name        = "read_only"
    description = "Groups for users allowed to view and inspect the tenancy configuration; for example, trainees"
    
    #Optional
    # defined_tags  = {"Operations.CostCenter"= "42"}
    freeform_tags   = {"Platform"=var.platform_name, "Environment"=var.label_prefix}
}

resource "oci_identity_policy" "read_only" {
    provider        = oci.home
    depends_on      = [ oci_identity_group.read_only ]

    #Required
    name           = "ReadOnly"
    description    = "Policies for the read only role"
    compartment_id = var.tenancy_id

    statements = ["ALLOW GROUP read_only to read all-resources IN TENANCY"]
}