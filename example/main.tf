// Run this file with the following command
// terraform apply -var tenancy_ocid=$OCI_TENANCY -auto-approve

# Retrieve the OCID for the root compartment from terraform command
variable "tenancy_ocid" { }

# Create a landing zone
module "landing_zone" {
  source     = "./modules/landing_zone"
  tenancy_id = var.tenancy_ocid
}

# Retrieve meta data for tenant
data "oci_identity_tenancy" "account" {
  tenancy_id = var.tenancy_ocid
}

# Output tenancy details
output "tenancy" {
  value = data.oci_identity_tenancy.account
}