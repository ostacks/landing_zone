# Create a resource compartments
resource "oci_identity_compartment" "admin_domain" {
    provider        = oci.home
    count           = length(local.activated)

    #Required
    compartment_id  = var.tenancy_id
    name            = local.activated[count.index]
    description     = "Compartment to manage ${local.activated[count.index]} resources"
    
    #Optional
    enable_delete   = false  // true will cause this compartment to be deleted when running `terrafrom destroy`
    # defined_tags  = {"Operations.CostCenter"= "42"}
    freeform_tags   = {"Platform"=var.platform_name, "Environment"=var.label_prefix}
}