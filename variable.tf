// retriev tenancy ocid
variable "tenancy_id" { }

// naming conventions
variable "platform_name" { default = "name" }
variable "label_prefix"    { default = "dev" }

// define network
variable "vcn_cidr" { default = "10.0.0.0/16" }

// user variable contains name, email, description and activates the account
variable "user_names" {
  description    = "user definition"
  type = list(object({
    name         = string
    email        = string
    description  = string
    active       = bool
    membership   = list(string)
  }))
  default = [
    {
    name         = "super_admin"
    email        = "ocilabs@mail.com"
    description  = "Administrator"
    active       = true
    membership   = ["tenant", "userid"]
    },
    {
    name         = "test_admin"
    email        = "ocilabs@mail.com"
    description  = "Test"
    active       = false
    membership   = ["userid"]
    }
  ]
}

# list of variables, reflecting service operation department structure
variable "departments" {
  type    = map(bool)
  description = "Resource compartments separate the technical management functions"
  default = {
      "management" = false
      "network"    = false
      "databases"  = false
      "services"   = false
      "internet"   = false
    }
}

# the base set of admin roles
variable "admins" {
  type    = map
  default = {
    "tenant"  = [
        "ALLOW GROUP tenant to read users IN TENANCY",
        "ALLOW GROUP tenant to read groups IN TENANCY",
        "ALLOW GROUP tenant to manage users IN TENANCY",
        "ALLOW GROUP tenant to manage groups IN TENANCY where target.group.name = 'Administrators'",
        "ALLOW GROUP tenant to manage groups IN TENANCY where target.group.name = 'security'",
    ]
    "userid"   = [
        "ALLOW GROUP userid to read users IN TENANCY",
        "ALLOW GROUP userid to read groups IN TENANCY",
        "ALLOW GROUP userid to manage users IN TENANCY",
        "ALLOW GROUP userid to manage groups IN TENANCY where all {target.group.name ! = 'Administrators', target.group.name ! = 'security'}",
    ]
    "system"  = [
        "ALLOW GROUP system to manage instance-family IN TENANCY where all {target.compartment.name=/*/, target.compartment.name!=/network/}",
        "ALLOW GROUP system to manage object-family IN TENANCY where all {target.compartment.name=/*/, target.compartment.name!=/network/}",
        "ALLOW GROUP system to manage volume-family IN TENANCY where all {target.compartment.name=/*/ , target.compartment.name!=/network/}",
        "ALLOW GROUP system to use load-balancers IN TENANCY where all {target.compartment.name=/*/ , target.compartment.name!=/network/}",
        "ALLOW GROUP system to use subnets IN TENANCY where target.compartment.name=/network/",
        "ALLOW GROUP system to use vnics IN TENANCY where target.compartment.name=/network/",
        "ALLOW GROUP system to use vnic-attachments IN TENANCY where target.compartment.name=/network/",
        "ALLOW GROUP system to manage compartments in Tenancy where all {target.compartment.name=/*/ , target.compartment.name!=/network/, target.compartment.name!=/shared/}",
        "ALLOW GROUP system to read all-resources IN TENANCY",
    ]
    "nas" = [
        "ALLOW GROUP nas to manage object-family IN TENANCY",
        "ALLOW GROUP nas to manage volume-family IN TENANCY",
        "ALLOW GROUP nas to read all-resources IN TENANCY",
    ]
    "database" = [
        "ALLOW GROUP database manage database-family IN TENANCY",
        "ALLOW GROUP database read all-resources IN TENANCY",
    ]
    "network" = [
        "ALLOW GROUP network to manage vcns IN TENANCY",
        "ALLOW GROUP network to manage subnets IN TENANCY",
        "ALLOW GROUP network to manage route-tables IN TENANCY",
        "ALLOW GROUP network to manage dhcp-options IN TENANCY",
        "ALLOW GROUP network to manage drgs IN TENANCY",
        "ALLOW GROUP network to manage cross-connects IN TENANCY",
        "ALLOW GROUP network to manage cross-connect-groups IN TENANCY",
        "ALLOW GROUP network to manage virtual-circuits IN TENANCY",
        "ALLOW GROUP network to manage vnics IN TENANCY",
        "ALLOW GROUP network to manage vnic-attachments IN TENANCY",
        "ALLOW GROUP network to manage load-balancers IN TENANCY",
        "ALLOW GROUP network to use virtual-network-family IN TENANCY",
        "ALLOW GROUP network to read all-resources IN TENANCY",
    ]
    "security" = [
        "ALLOW GROUP security to manage security-lists IN TENANCY",
        "ALLOW GROUP security to manage internet-gateways IN TENANCY",
        "ALLOW GROUP security to manage cpes IN TENANCY",
        "ALLOW GROUP security to manage ipsec-connections IN TENANCY",
        "ALLOW GROUP security to use virtual-network-family IN TENANCY",
        "ALLOW GROUP security to manage load-balancers IN TENANCY",
        "ALLOW GROUP security to read all-resources IN TENANCY",
    ]
  }
}